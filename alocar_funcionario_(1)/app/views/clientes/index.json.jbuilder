json.array!(@clientes) do |cliente|
  json.extract! cliente, :id, :nome, :local
  json.url cliente_url(cliente, format: :json)
end
