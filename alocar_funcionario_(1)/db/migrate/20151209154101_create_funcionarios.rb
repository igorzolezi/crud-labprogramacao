class CreateFuncionarios < ActiveRecord::Migration
  def change
    create_table :funcionarios do |t|
      t.string :nome
      t.string :registro
      t.string :setor
      t.integer :cliente_id

      t.timestamps null: false
    end
  end
end
